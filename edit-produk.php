<?php

    require '../koneksinya.php'; // Memanggil koneksi database

    $getId      = $_GET['id_produk'];

    $query      = "SELECT * FROM produk WHERE id_produk='$getId'";
    $mysqlQuery = mysqli_query($koneksinya, $query);
    $result     = mysqli_fetch_assoc($mysqlQuery);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">

    <meta name="googlebot" content="index,follow">
    <meta name="googlebot-news" content="index,follow">
    <meta name="robots" content="index,follow">
    <meta name="Slurp" content="all">

    <title>Linda - Kasir Minimarket</title>

    <link rel="stylesheet" href="../plugins/css/bootstrap.css">

    <link rel="stylesheet" href="../plugins/font-awesome-4.7.0/css/font-awesome.min.css">
</head>

<body>

    <div class="container-fluid bg-warning">
        <div class="container p-0">

            <nav class="navbar px-0 navbar-expand-md bg-warning navbar-dark">
                <div class="container">
                    <!-- Brand/logo -->
                    <a class="navbar-brand" href="#">kasirMINIMARKET</a>

                    <div class="justify-content-end d-block d-lg-none">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdownMenu" aria-controls="navbarNavDropdownMenu" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>

                    <!-- Links -->
                    <div class="collapse navbar-collapse font-weight-semi-bold justify-content-end" id="navbarNavDropdownMenu">
                        <ul class="navbar-nav">
                            <li class="nav-item p-2">
                                <a class="nav-link" href="index.php">Index</a>
                            </li>
                            <li class="nav-item p-2">
                                <a class="nav-link" href="rekap-data-penjualan.php">Rekap Data Penjualan</a>
                            </li>
                            <li class="nav-item p-2">
                                <a class="nav-link" href="daftar-produk.php">Daftar Produk</a>
                            </li>
                            <li class="nav-item p-2">
                                <a class="nav-link" href="tambah-produk.php">Tambah Produk</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

        </div>
    </div>

    <!-- ISINYA -->

    <div class="container my-4 text-center">
        <div class="row justify-content-center">
            <div class="col-8">
                <h3>Edit Produk <u><?php echo $result['nama']; ?></u></h3>
                <br />
                <form method="POST" action="memproses_edit_produk.php">
                    <div class="form-group">
                        <label for="idProduk">ID Produk</label>
                        <input type="text" class="form-control" id="idProduk" name="idProduk" value="<?php echo $result['id_produk']; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="namaProduk">Nama Produk</label>
                        <input type="text" class="form-control" id="namaProduk" name="namaProduk" value="<?php echo $result['nama']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="hargaProduk">Harga Produk</label>
                        <input type="number" class="form-control" id="hargaProduk" name="hargaProduk" value="<?php echo $result['harga']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="stockProduk">Stock Produk</label>
                        <input type="number" class="form-control" id="stockProduk" name="stockProduk" value="<?php echo $result['stock']; ?>">
                    </div>
                    <button type="submit" class="btn btn-lg btn-block btn-warning">EDIT PRODUK</button>
                </form>
            </div>
        </div>
    </div>

    <!-- ISINYA -->

    <div class="container-fluid text-center py-2 bg-warning">
        <small>&copy;Dibuat oleh LINDA</small>
    </div>

    <script src="../plugins/js/jquery.min.js"></script>
    <script src="../plugins/js/popper.js"></script> 
    <script src="../plugins/js/bootstrap.min.js"></script>
    <script src="../plugins/js/aos.js"></script>

    <script type="text/javascript">

        // Popover
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();   
        });
        // Popover

    </script>

</body>
</html>