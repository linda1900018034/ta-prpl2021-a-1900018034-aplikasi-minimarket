<?php

    require '../koneksinya.php'; // Memanggil koneksi database
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">

    <meta name="googlebot" content="index,follow">
    <meta name="googlebot-news" content="index,follow">
    <meta name="robots" content="index,follow">
    <meta name="Slurp" content="all">

    <title>Linda - Kasir Minimarket</title>

    <link rel="stylesheet" href="../plugins/css/bootstrap.css">

    <link rel="stylesheet" href="../plugins/font-awesome-4.7.0/css/font-awesome.min.css">
</head>

<body>

    <div class="container-fluid bg-warning">
        <div class="container p-0">

            <nav class="navbar px-0 navbar-expand-md bg-warning navbar-dark">
                <div class="container">
                    <!-- Brand/logo -->
                    <a class="navbar-brand" href="#">kasirMINIMARKET</a>

                    <div class="justify-content-end d-block d-lg-none">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdownMenu" aria-controls="navbarNavDropdownMenu" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>

                    <!-- Links -->
                    <div class="collapse navbar-collapse font-weight-semi-bold justify-content-end" id="navbarNavDropdownMenu">
                        <ul class="navbar-nav">
                            <li class="nav-item p-2">
                                <a class="nav-link" href="index.php">Index</a>
                            </li>
                            <li class="nav-item p-2">
                                <a class="nav-link" href="rekap-data-penjualan.php">Rekap Data Penjualan</a>
                            </li>
                            <li class="nav-item p-2">
                                <a class="nav-link" href="daftar-produk.php">Daftar Produk</a>
                            </li>
                            <li class="nav-item p-2">
                                <a class="nav-link" href="tambah-produk.php">Tambah Produk</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

        </div>
    </div>

    <!-- ISINYA -->

    <?php

        // Menghitung banyaknya produk
        $query1    = "SELECT SUM(stock) AS stock FROM produk";
        $action1   = mysqli_query($koneksinya, $query1);
        $result1   = mysqli_fetch_assoc($action1);

        // Menghitung produk stock kosong
        $query2    = "SELECT COUNT(stock) AS stock_kosong FROM produk WHERE stock='0'";
        $action2   = mysqli_query($koneksinya, $query2);
        $result2   = mysqli_fetch_assoc($action2);

        // Menghitung produk terjual
        $query3    = "SELECT COUNT(id_penjualan) AS produk_terjual FROM penjualan";
        $action3   = mysqli_query($koneksinya, $query3);
        $result3   = mysqli_fetch_assoc($action3);

    ?>

    <div class="container my-4 text-center">
        <div class="row">
            <div class="col-4 border py-2 text-warning border-warning shadow">
                <h3>Terdapat Stok Produk</h3>
                <h1><?php echo $result1['stock']; ?></h1>
            </div>
            <div class="col-4 border py-2 text-warning border-warning shadow">
                <h3>Terdapat Stok Produk Yang Kosong</h3>
                <h1><?php echo $result2['stock_kosong']; ?></h1>
            </div>
            <div class="col-4 border py-2 text-warning border-warning shadow">
                <h3>Total Produk Yang Terjual</h3>
                <h1><?php echo $result3['produk_terjual']; ?></h1>
            </div>
        </div>
    </div>

    <!-- ISINYA -->

    <div class="container-fluid text-center py-2 bg-warning">
            <small>&copy;Dibuat oleh LINDA</small>
        </div>
    </div>

    <script src="../plugins/js/jquery.min.js"></script>
    <script src="../plugins/js/popper.js"></script> 
    <script src="../plugins/js/bootstrap.min.js"></script>
    <script src="../plugins/js/aos.js"></script>

    <script type="text/javascript">

        // Popover
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();   
        });
        // Popover

    </script>

</body>
</html>